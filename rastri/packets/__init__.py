import datetime


def now_str():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


class Packet:
    def __init__(self, entity_id):
        self.entity_id = entity_id
        self.generated_at = now_str()

        self._coordinates = None
        self.altitude = None
        self.converted_at = None
        self.device_id = 0
        self.payload = {'test': True}

    @property
    def coordinates(self):
        return self._coordinates

    @property
    def coordinates_wkt(self):
        x, y = self._coordinates
        return f'POINT ({x} {y})'

    @coordinates.setter
    def coordinates(self, value):
        # TODO: check validity of the coordinates pair
        # It should be a valid Point WKT
        self._coordinates = value

    def as_dict(self):
        self.converted_at = now_str()

        return {
            'coordinates': self.coordinates_wkt,
            'entity_id': self.entity_id,
            'altitude': self.altitude,
            'generated_at': self.generated_at,
            'converted_at': self.converted_at,
            'device_id': self.device_id,
            'payload': self.payload
        }

    def __repr__(self):
        clsname = self.__class__.__name__
        return f'<{clsname}: {self}>'

    def __str__(self):
        representation = []

        if self.entity_id:
            representation.append(f'{self.entity_id}:')

        if self.coordinates:
            representation.append(f'{self.coordinates}')

        if self.altitude:
            representation.append(f'altitude:{self.coordinates}m')

        if self.device_id:
            representation.append(f'device:{self.device_id}')

        return ' '.join(representation)
