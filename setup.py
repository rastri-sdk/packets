try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = '0.0.3'

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='rastri-sdk-packets',
    version=version,
    description="Packets basic models and utilities",
    long_description=readme,
    author='Cléber Zavadniak',
    author_email='cleber@rastri.com.br',
    url='https://gitlab.com/rastri-sdk/packets',
    license=license,
    packages=['rastri.packets'],
    package_data={'': ['README.md']},
    include_package_data=True,
    install_requires=[],
    zip_safe=True,
    keywords='generic libraries',
    classifiers=(
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ),
)
